package server;

import message.Message;
import message.MessageType;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import java.util.List;

/**
 * Created by master on 08.07.2015.
 */
public class Server {

    public static void main(String[] args)
    {
        new Server();
    }

    private JFrame serverFrame;
    private boolean started;
    private boolean online;
    private ServerSocket server;
    private Database db;
    private int port;
    private List<Connection> connections = Collections.synchronizedList(new ArrayList<Connection>());

    public Server()
    {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                initComponents();
            }
        });

        while(!started)
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {}

        try {
            db = new Database();
        } catch (Exception e) {}

        try {
            server = new ServerSocket(port);
        } catch (IOException e) {}
        online = true;

        Thread onlineUsers = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {}
                    synchronized (connections) {
                        for (Connection x : connections)
                            try {
                                x.out.writeObject(new Message(MessageType.ONLINE_USERS, getOnlineUsers()));
                            } catch (Exception e) {
                            }
                    }
                }
            }
        });
       onlineUsers.start();

        while (online)
        {
            try {
                Connection connection = new Connection(server.accept());
                connection.start();
                connections.add(connection);
            } catch (IOException e) {}
        }

    }

    public class Connection extends Thread
    {
        private ObjectOutputStream out;
        private ObjectInputStream in;
        private Socket socket;
        private String name;

        Connection(Socket socket)
        {
            this.socket = socket;
            try {
                out = new ObjectOutputStream(socket.getOutputStream());
                out.flush();
                in = new ObjectInputStream(socket.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        public void run()
        {
            Message message;
            while (true)
            {
                try {
                    message = (Message) in.readObject();
                    if (message.messageType == MessageType.LOGIN)
                        if (db.login(message.login, message.password)) {
                            out.writeObject(new Message(MessageType.LOGGED_IN, getOnlineUsers()));
                            name = message.login;
                            break;
                        } else {
                            out.writeObject(new Message(MessageType.AUTHORIZATION_ERROR));
                        }
                    if (message.messageType == MessageType.REGISTRATION)
                        if (db.registerNewUser(message.login, message.password)) {
                            out.writeObject(new Message(MessageType.LOGGED_IN, getOnlineUsers()));
                            name = message.login;
                            break;
                        } else {
                            out.writeObject(new Message(MessageType.AUTHORIZATION_ERROR));
                        }
                }catch (Exception e){System.out.println(e.toString());}
            }

            while (true)
            {
                try {
                    message = (Message)in.readObject();
                    synchronized (connections) {
                        for (Connection x : connections) {
                            if (x.name.equals(message.receiver)) {
                                x.out.writeObject(message);
                                break;
                            }
                        }
                    }
                }catch (Exception e){}
            }
        }
        void close() throws IOException {
            in.close();
            out.close();
            socket.close();
            connections.remove(this);
        }
    }
    public ArrayList<String> getOnlineUsers()
    {
        ArrayList<String> onlineUsers = new ArrayList<String>();
        for(Connection x : connections) {
            onlineUsers.add(x.name);
        }
        return onlineUsers;
    }

    void closeAll() throws IOException {
        synchronized (connections) {
            Iterator<Connection> iterator = connections.iterator();
            while (iterator.hasNext()) {
                iterator.next().close();
            }
        }
        server.close();
    }
    void initComponents()
    {
        serverFrame = new JFrame("Server");
        serverFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JPanel panel  =  new JPanel();
        panel.setPreferredSize(new Dimension(250, 105));
        Font font = new Font("Arial", Font.BOLD, 20);

        JPanel serverStatusPanel = new JPanel(new BorderLayout());
        JLabel labelServerStatus1 = new JLabel("Server status: ");
        final JLabel labelServerStatus2 = new JLabel("OFFLINE");
        labelServerStatus1.setFont(font);
        labelServerStatus2.setFont(font);
        labelServerStatus2.setForeground(Color.gray);
        serverStatusPanel.add(labelServerStatus1, BorderLayout.WEST);
        serverStatusPanel.add(labelServerStatus2, BorderLayout.EAST);

        panel.add(serverStatusPanel);

        JPanel PortPanel = new JPanel(new BorderLayout());
        JLabel labelPort = new JLabel("Port: ");
        final JTextField textPort = new JTextField();
        textPort.setFont(font);
        textPort.setPreferredSize(new Dimension(65, 20));
        labelPort.setFont(font);
        PortPanel.add(labelPort, BorderLayout.WEST);
        PortPanel.add(textPort, BorderLayout.EAST);

        final JButton button = new JButton("Start server");
        button.setFont(new Font("Arial", font.BOLD, 25));
        button.setForeground(new Color(0, 130, 0));
        button.setPreferredSize(new Dimension(200, 45));
        panel.add(serverStatusPanel);
        panel.add(PortPanel);
        panel.add(button);
        serverFrame.getContentPane().add(panel);
        serverFrame.pack();
		try{
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }catch(Exception e){}
		serverFrame.setVisible(true);
        serverFrame.setLocationRelativeTo(null);
        serverFrame.setResizable(false);

        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(!textPort.getText().equals(""))
                {
                    if(online)
                    {
                        try {
                            online = false;
                            closeAll();
                            System.exit(0);
                        } catch (IOException e1) {}
                    }
                    else if(!online)
                    {
                        started = true;
                        port = Integer.parseInt(textPort.getText());
                        while(!online)
                            try {
                                Thread.sleep(500);
                            } catch (InterruptedException e1){}

                        textPort.setEditable(false);
                        labelServerStatus2.setText("ONLINE");
                        labelServerStatus2.setForeground(new Color(0, 130, 0));
                        button.setText("Stop server");
                        button.setForeground(new Color(210, 0, 0));
                    }
                }
            }
        });
    }
}
