package server;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

/**
 * Created by master on 04.07.2015.
 */
public class Database
{
    private java.sql.Connection connection = null;
    private Statement statement;

    Database() throws Exception
    {

        Properties properties = new Properties();
        properties.put("user", "");
        properties.put("password", "");
        Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
        String db = "jdbc:odbc:Driver={Microsoft Access Driver (*.mdb, *.accdb)};DBQ=C:\\Users\\master\\Desktop\\database\\Database.mdb";
        this.connection = DriverManager.getConnection(db, properties);

        System.out.println("Connected to database...");
    }
    public boolean login(String login, String password) throws Exception
    {
        String hashPass = getMd5Hash(password);
        statement = connection.createStatement();
        ResultSet rs = statement.executeQuery("SELECT * FROM data WHERE data.login = '" + login + "'AND data.hashpass = '" + hashPass + "'");
        return rs.next()?true:false;
    }
    public boolean registerNewUser(String login, String password) throws Exception{
        if(!loginIsUnique(login))
            return false;

        String hashPass = getMd5Hash(password);
        statement = connection.createStatement();
        statement.executeUpdate("INSERT INTO data (login, hashpass) VALUES ('"+login+"', '"+hashPass+"')");
        return true;
    }
    public ArrayList<String> getAllUsers() throws Exception
    {
        ArrayList<String>list = new ArrayList<String>();
        statement = connection.createStatement();
        ResultSet rs = statement.executeQuery("SELECT data.login FROM data");
        while (rs.next())
        {
            list.add(rs.getString(1));
        }
        return list;
    }
    private boolean loginIsUnique(String login) throws Exception
    {
        statement = connection.createStatement();
        ResultSet rs = statement.executeQuery("SELECT * FROM data WHERE data.login = " + "'"+login+"'");
        return rs.next()?false:true;
    }
    private String getMd5Hash(String password) throws Exception {

        MessageDigest md = MessageDigest.getInstance("MD5");
        md.reset();
        md.update(password.getBytes());
        byte[] digest = md.digest();
        BigInteger bigInt = new BigInteger(1, digest);
        String hashPass = bigInt.toString(16);
        return hashPass;
    }
}