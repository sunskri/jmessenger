package message;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by master on 08.07.2015.
 */
public class Message implements Serializable {

    public MessageType messageType;
    public String login;
    public String password;
    public String sender;
    public String receiver;
    public String messageText;
    public ArrayList<String> onlineUsers;

    public Message(MessageType messageType)
    {
        this.messageType = messageType;
    }
    public Message(MessageType messageType, ArrayList<String> onlineUsers)
    {
        this.messageType = messageType;
        this.onlineUsers = onlineUsers;
    }
    public Message(MessageType messageType, String login, String password)
    {
        this.messageType = messageType;
        this.login = login;
        this.password = password;
    }
    public Message(MessageType messageType, String sender, String receiver, String messageText)
    {
        this.messageType = messageType;
        this.sender = sender;
        this.receiver = receiver;
        this.messageText = messageText;
    }
}
