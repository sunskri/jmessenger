package message;

/**
 * Created by master on 08.07.2015.
 */
public enum MessageType {
    MESSAGE,
    LOGIN,
    REGISTRATION,
    LOGGED_IN,
    AUTHORIZATION_ERROR,
    ONLINE_USERS
}
