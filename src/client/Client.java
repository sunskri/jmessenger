package client;

import message.Message;
import message.MessageType;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by master on 08.07.2015.
 */
public class Client {

    public static void main(String[] args)
    {
        new Client();
    }
    private String name;
    private Socket socket;
    private ObjectOutputStream out;
    private ObjectInputStream in;
    private boolean connected;
    private volatile boolean logged_in;
    private volatile boolean authorization_error;
    private JFrame connectionFrame;
    private JFrame loginFrame;
    private JFrame mainFrame;
    private JTextArea textArea;
    private JList list;
    private HashMap<String, String> usersAndMessages = new HashMap<String, String>();

    public Client()
    {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                initComponents();
            }
        });

        while(!connected)
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {}

        Message message;

        while (!logged_in)
        {
            try {
                message = (Message)in.readObject();
                if(message.messageType == MessageType.LOGGED_IN) {
                    logged_in = true;
                    setOnlineUsers(message.onlineUsers);
                }
                if(message.messageType == MessageType.AUTHORIZATION_ERROR) {
                    authorization_error = true;
                }
            } catch (Exception e) {}
        }

        while (true)
        {
            try {
                message = (Message)in.readObject();
                if(message.messageType == MessageType.ONLINE_USERS)
                    setOnlineUsers(message.onlineUsers);

                if(message.messageType == MessageType.MESSAGE)
                {
                    if(message.sender.equals(list.getSelectedValue())) {
                        textArea.append(message.sender + ": " + message.messageText + "\n");
                        textArea.setCaretPosition(textArea.getDocument().getLength());
                    }

                    usersAndMessages.put(message.sender, usersAndMessages.get(message.sender) + message.sender + ": " + message.messageText + "\n");
                }
            }catch (Exception e){}
        }
    }
    void setOnlineUsers(ArrayList<String> onlineUsers)
    {
        if(onlineUsers.size() == 1)
            return;

        DefaultListModel model = (DefaultListModel) list.getModel();
        for(int i = 0; i < model.getSize(); i++)
        {
            if(!onlineUsers.contains(model.get(i))) {
                ((DefaultListModel) list.getModel()).removeElement(model.get(i));
                usersAndMessages.remove(model.get(i));
            }
        }
        for(String x : onlineUsers)
        {
            if(!model.contains(x))
            {
                if(x.equals(name))
                    continue;

                ((DefaultListModel) list.getModel()).addElement(x);
                usersAndMessages.put(x, "");
            }
        }
    }
    void initComponents()
    {
        //////////////////////////////////////////////////////
        //////////MAIN FRAME

            mainFrame = new JFrame("JMessenger");
            mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            JPanel panel = new JPanel();
            panel.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
            panel.setLayout(new BorderLayout());

            textArea = new JTextArea();
            textArea.setLineWrap(true);
            textArea.setWrapStyleWord(true);
            textArea.setEditable(false);
            textArea.setBorder(BorderFactory.createLineBorder(Color.gray));
            JScrollPane textscroll = new JScrollPane(textArea);
            textscroll.setPreferredSize(new Dimension(200, 200));
            final DefaultListModel listModel = new DefaultListModel();
            list = new JList(listModel);
            list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            list.setBorder(BorderFactory.createLineBorder(Color.gray));
            final JScrollPane listscroll = new JScrollPane(list);
            listscroll.setPreferredSize(new Dimension(100, 200));
            panel.add(textscroll, BorderLayout.CENTER);
            panel.add(listscroll, BorderLayout.EAST);

            JPanel panel2 = new JPanel();
            panel2.setLayout(new BorderLayout());
            final JTextField text = new JTextField();
            text.setPreferredSize(new Dimension(200, 23));
            text.setBorder(BorderFactory.createLineBorder(Color.gray));
            JButton button = new JButton("Send");
            button.setPreferredSize(new Dimension(100, 20));
            panel2.add(text, BorderLayout.CENTER);
            panel2.add(button, BorderLayout.EAST);
            panel.add(panel2, BorderLayout.SOUTH);
            mainFrame.getContentPane().add(panel);
            mainFrame.setPreferredSize(new Dimension(400, 350));
            mainFrame.setMinimumSize(new Dimension(330, 300));
            mainFrame.pack();
            mainFrame.setLocationRelativeTo(null);
            text.requestFocus();

            text.addKeyListener(new KeyAdapter(){

                public void keyReleased( KeyEvent e ){

                    if(e.getKeyCode() == KeyEvent.VK_ENTER && !text.getText().equals(""))
                    {
                        try {
                            out.writeObject(new Message(MessageType.MESSAGE, name, (String)list.getSelectedValue(), text.getText()));
                            textArea.append(name + ": " + text.getText() + "\n");
                            textArea.setCaretPosition(textArea.getDocument().getLength());
                            usersAndMessages.put((String) list.getSelectedValue(), textArea.getText());
                        } catch (IOException e1) {}
                        text.setText("");
                    }
                }});
            button.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    if(!text.getText().equals(""))
                    {
                        try {
                            out.writeObject(new Message(MessageType.MESSAGE, name, (String)list.getSelectedValue(), text.getText()));
                            textArea.append(name + ": " + text.getText() + "\n");
                            textArea.setCaretPosition(textArea.getDocument().getLength());
                            usersAndMessages.put((String)list.getSelectedValue(), textArea.getText());
                        } catch (IOException e1) {}
                        text.setText("");
                        text.requestFocus();
                    }
                    text.requestFocus();
                }
            });

            list.addListSelectionListener(new ListSelectionListener() {
                @Override
                public void valueChanged(ListSelectionEvent e) {
                    if(!e.getValueIsAdjusting())
                    {
                        textArea.setText(usersAndMessages.get(list.getSelectedValue()));
                    }
                }
            });

        //////////////////////////////////////////////////////
        //////////CONNECTION FRAME
        connectionFrame = new JFrame("Connection to server");
        connectionFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Font font = new Font("Arial",Font.BOLD,15);
        JPanel mfpanel = new JPanel();
        JLabel labelPort = new JLabel("Port:");
        labelPort.setFont(font);
        JLabel labelIp = new JLabel("IP:");
        labelIp.setFont(font);
        final JTextField textPort = new JTextField();
        textPort.setFont(font);
        textPort.setPreferredSize(new Dimension(46, 20));
        final JTextField textIp = new JTextField();
        textIp.setFont(font);
        textIp.setPreferredSize(new Dimension(115, 20));
        JButton _button = new JButton("Connect");
        mfpanel.add(labelPort);
        mfpanel.add(textPort);
        mfpanel.add(labelIp);
        mfpanel.add(textIp);
        mfpanel.add(_button);
        mfpanel.setPreferredSize(new Dimension(250, 50));
        connectionFrame.getContentPane().add(mfpanel);
        connectionFrame.pack();
        connectionFrame.setLocationRelativeTo(null);
        connectionFrame.setResizable(false);
        connectionFrame.setVisible(true);
        _button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(!textIp.getText().equals("") && !textPort.getText().equals(""))
                {
                    try {
                        socket = new Socket(textIp.getText(), Integer.parseInt(textPort.getText()));
                        out = new ObjectOutputStream(socket.getOutputStream());
                        in = new ObjectInputStream(socket.getInputStream());
                        connected = true;
                        connectionFrame.dispose();
                        loginFrame.setVisible(true);
                    } catch (IOException e1) {}
                }
            }
        });
        //////////////////////////////////////////////////////
        //////////LOGIN FRAME
        loginFrame = new JFrame("Login or Register");
        loginFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JPanel _panel  =  new JPanel();
        JLabel labelLogin = new JLabel("Login: ");
        final JTextField textLogin = new JTextField();
        textLogin.setPreferredSize(new Dimension(100, 20));
        JLabel labelPass = new JLabel("Password: ");
        final JPasswordField textPass = new JPasswordField();
        textPass.setPreferredSize(new Dimension(100, 20));
        JButton loginButton = new JButton("Login");
        JButton registerButton = new JButton("Register");
        _panel.add(labelLogin);
        _panel.add(textLogin);
        _panel.add(labelPass);
        _panel.add(textPass);
        _panel.add(loginButton);
        _panel.add(registerButton);
        _panel.setPreferredSize(new Dimension(100, 157));
        loginFrame.getContentPane().add(_panel);
        loginFrame.pack();
        loginFrame.setResizable(false);
        loginFrame.setLocationRelativeTo(null);
        loginButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!textLogin.getText().equals("") && !textPass.getText().equals(""))
                    try {
                        out.writeObject(new Message(MessageType.LOGIN, textLogin.getText(), textPass.getText()));
                        while (!logged_in || !authorization_error) {
                            if (logged_in) {
                                mainFrame.setVisible(true);
                                name = textLogin.getText();
                                loginFrame.dispose();
                                break;
                            }
                            if (authorization_error) {
                                authorization_error = false;
                                break;
                            }
                        }
                    } catch (IOException e1) {
                    }
            }
        });

        registerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(!textLogin.getText().equals("") && !textPass.getText().equals("")) {
                    try {
                        out.writeObject(new Message(MessageType.REGISTRATION, textLogin.getText(), textPass.getText()));
                        while (!logged_in || !authorization_error) {
                            if (logged_in) {
                                mainFrame.setVisible(true);
                                name = textLogin.getText();
                                loginFrame.dispose();
                                break;
                            }
                            if (authorization_error) {
                                authorization_error = false;
                                break;
                            }
                        }
                    } catch (IOException e1) {
                    }
                }
            }
        });
    }

}
